webpackJsonp(["layout.module"],{

/***/ "./src/app/layout-footer/layout-footer.component.css":
/***/ (function(module, exports) {

module.exports = "/*Ejemplo de css*/\r\n.ejemplo{\r\n    background-color: aliceblue;\r\n}\r\n:host{\r\n    position: fixed;\r\n    bottom: 0;\r\n    display: inline;\r\n    padding: 0;\r\n    margin: 0;\r\n    z-index: 1500;\r\n    width: 100%;\r\n    left: 0;\r\n    background-color: #6F7779;\r\n}\r\n:host .main-footer div{\r\n    color: #fff;\r\n}\r\n:host .main-footer{\r\n    background-color: #6F7779;\r\n    padding-top: 2px;\r\n}"

/***/ }),

/***/ "./src/app/layout-footer/layout-footer.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Main Footer -->\r\n<footer class=\"main-footer\">\r\n      <!-- To the right -->\r\n      <div class=\"pull-right hidden-xs text-center align-center\">\r\n          <strong > &copy; {{constants.FOOTER_COPYRIGHT_YEAR}} <a  class=\"text-white\">{{constants.FOOTER_LOGO}}</a></strong>\r\n      </div>\r\n      <!-- Default to the left -->\r\n      \r\n    </footer>"

/***/ }),

/***/ "./src/app/layout-footer/layout-footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LayoutFooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__model_constants__ = __webpack_require__("./src/model/constants.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LayoutFooterComponent = /** @class */ (function () {
    function LayoutFooterComponent() {
        this.constants = __WEBPACK_IMPORTED_MODULE_1__model_constants__["a" /* Constants */];
    }
    LayoutFooterComponent.prototype.ngOnInit = function () {
    };
    LayoutFooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-layout-footer',
            template: __webpack_require__("./src/app/layout-footer/layout-footer.component.html"),
            styles: [__webpack_require__("./src/app/layout-footer/layout-footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LayoutFooterComponent);
    return LayoutFooterComponent;
}());



/***/ }),

/***/ "./src/app/layout-header/layout-header.component.css":
/***/ (function(module, exports) {

module.exports = "/*Ejemplo de css*/\r\n.navbar-static-top{\r\n    padding: 0;\r\n    background-color: #fff;\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-pack: center;\r\n        -ms-flex-pack: center;\r\n            justify-content: center;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n}\r\nh3{\r\n    margin: 0;\r\n}\r\n/*Alineacion vertical de objetos*/\r\n.valign-c{\r\n    line-height: 50px;\r\n}\r\n.nav > li > a{\r\n    color: #888;\r\n    display: inline;\r\n    border: 0;\r\n    padding: 5px;\r\n}\r\n.nav > li > a:hover{\r\n    color: #888;\r\n    background-color: transparent;\r\n}\r\n.nav > li > a:visited{\r\n    color: #888;\r\n    background-color: transparent;\r\n}\r\n.color-gray{\r\n    color: #888;\r\n}\r\n.fa-regular{\r\n    font-size: 1.2em;\r\n}\r\n.align-r{\r\n    right: 0px;\r\n    left: auto;\r\n    min-width: 30rem;\r\n}\r\n.column-r{\r\n    -webkit-box-orient: initial;\r\n    -webkit-box-direction: initial;\r\n        -ms-flex-direction: initial;\r\n            flex-direction: initial;\r\n}\r\n.navbar-nav>.tasks-menu>.dropdown-menu>li.footer>a{\r\n    padding: 2px;\r\n    -webkit-text-decoration-line: underline;\r\n            text-decoration-line: underline;\r\n}\r\n.table td{\r\n    padding: 0.25rem;\r\n}\r\n.size-12{\r\n    font-size: 12px;\r\n}\r\n.size-10{\r\n    font-size: 10px;\r\n}\r\n.border-rg{\r\n    border-right: 1px solid #bbb;\r\n}\r\n.logo span{\r\n    color: white;\r\n}\r\n.logo{\r\n    background-color: #EC0000;\r\n}\r\n.logo:hover{\r\n    background-color: #EC0000;\r\n}\r\n.fs-32{\r\n    font-size: 32px;\r\n}\r\n.fs-56{\r\n    margin-top: -5px;\r\n    font-size: 56px;\r\n}\r\n.size-a{\r\n    font-size: 0.8rem;\r\n}\r\n.h-40-px{\r\n    height: 40px;\r\n}\r\n.h-45-px{\r\n    height: 45px;\r\n}\r\n.border-bottom-gray{\r\n    border-bottom: 3px solid #d2d6de;\r\n}\r\n/*Borde a 3 pixeles gris*/\r\n.border-3px{\r\n    border-right: 2px solid #bbb;\r\n}\r\n.logo>.logo-mini{\r\n    background-color: white;\r\n}\r\n.logo>.logo-lg{\r\n    background-color: black;\r\n}\r\n.logo{\r\n    background-color: transparent;\r\n}\r\n.logo:hover{\r\n    background-color: transparent;\r\n}"

/***/ }),

/***/ "./src/app/layout-header/layout-header.component.html":
/***/ (function(module, exports) {

module.exports = "<header class=\"main-header\">\r\n  <div class=\"logo\" >\r\n    <span class=\"logo-mini fs-32\" alt=\"logo-ini\">\r\n      <img src=\"assets/Images/logo_min.png\" width=\"50\" height=\"50\" ckass>\r\n    </span>\r\n    <span class=\"logo-lg fs-56\"  alt=\"logo-ini large\">\r\n        <img src=\"assets/Images/logo_lg.png\" width=\"200\" height=\"50\">\r\n    </span>\r\n  </div>\r\n  <nav class=\"navbar navbar-static-top bg-white\" role=\"navigation\">\r\n    <div class=\"col-3 \">\r\n      <a class=\"bg-transparent sidebar-toggle text-dark align-middle d-inline\" data-toggle=\"push-menu\"  role=\"button\">\r\n        <span class=\"sr-only\">Toggle navigation</span>\r\n      </a>\r\n      <div class=\"\">\r\n        <h3 class=\"text-danger valign-c\">{{constants.HEADER_TITLE}}</h3>\r\n      </div>\r\n\r\n    </div>\r\n    <div class=\"col-9\">\r\n        <div class=\"navbar-custom-menu\">\r\n            <ul class=\"nav navbar-nav column-r\">\r\n              \r\n              <!-- Control Sidebar Toggle Button -->\r\n              <li class=\"border-rg\">\r\n                <span class=\"size-12 align-middle pl-3 pr-3 font-weight-bold color-gray d-block\">ADRIAN GALAN ARRIAGA</span>\r\n                <span class=\"size-10 align-middle pl-3 pr-3 font-weight-bold text-red d-block float-right\">{{fecha}}</span>\r\n              </li>\r\n                <!-- Control Sidebar Toggle Button -->\r\n              <li class=\" pl-2 pr-2\">\r\n                <a href=\"#\" data-toggle=\"control-sidebar\"><i class=\"fa fa-regular fa-power-off align-middle \"></i></a>\r\n              </li>\r\n            </ul>\r\n          </div>\r\n    </div>\r\n    <section class=\"content-header p-0 w-100\" >\r\n        <div class=\"w-100 row m-0 border-bottom-gray h-45-px bg-san-g justify-content-end\" >\r\n          \r\n            <div class=\"w-20 border-3px  h-40-px col-5\" *ngIf=\"true\">\r\n              </div>\r\n          <div class=\"w-20 border-3px  h-40-px col-2\" *ngIf=\"true\">\r\n            <a class=\"w-100 text-center btn color-gray size-a\" role=\"button\" href=\"\">ADMINSTRACIÓN</a>\r\n          </div>\r\n          <div class=\"w-20 border-3px h-40-px col-5\" *ngIf=\"true\">\r\n          </div>\r\n            <div class=\"w-20 border-3px p-1 m-1 h-40-px\" *ngIf=\"false\">\r\n              <a class=\"w-100 text-center btn color-gray size-a\" role=\"button\" href=\"\" >FISCAL</a>\r\n            </div>\r\n            <div class=\"w-20 border-3px p-1 m-1 h-40-px\" *ngIf=\"false\">\r\n              <a class=\"w-100 text-center btn color-gray size-a\" role=\"button\" href=\"\">EMISIÓN</a>\r\n            </div>\r\n            <div class=\"w-20 border-3px p-1 m-1 h-40-px\" *ngIf=\"false\">\r\n              <a class=\"w-100 text-center btn color-gray size-a\" role=\"button\" href=\"\" >RECEPCIÓN</a>\r\n            </div>\r\n            <div class=\"w-20 border-3px p-1 m-1 h-40-px\" *ngIf=\"false\">\r\n              <a class=\"w-100 text-center btn color-gray size-a m-1 h-40-px\" role=\"button\" href=\"\" >SEGURIDAD</a>\r\n            </div>\r\n        </div>\r\n        \r\n      </section>\r\n    <div class=\"m-0 w-100 bg-san-g\">\r\n        <router-outlet></router-outlet>\r\n    </div>\r\n  </nav>\r\n  \r\n  \r\n    \r\n</header>\r\n<div class=\"col-12 p-0 m-0\" >\r\n    \r\n<section class=\"content container-fluid p-0\" >\r\n    \r\n</section>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/layout-header/layout-header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LayoutHeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_config__ = __webpack_require__("./src/app/app.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_constants__ = __webpack_require__("./src/model/constants.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LayoutHeaderComponent = /** @class */ (function () {
    function LayoutHeaderComponent(config) {
        this.config = config;
        this.constants = __WEBPACK_IMPORTED_MODULE_2__model_constants__["a" /* Constants */];
        this.value = 30;
        this.fechaFormat = new Date();
    }
    LayoutHeaderComponent.prototype.ngOnInit = function () {
        var month = this.fechaFormat.getMonth() + 1;
        var month_text = month + "";
        var day = this.fechaFormat.getDate();
        var day_text = day + "";
        if (month_text.length < 2) {
            month_text = "0" + month_text;
        }
        if (day_text.length < 2) {
            day_text = "0" + day_text;
        }
        this.fecha = this.fechaFormat.getFullYear() + "-" + month_text + "-" + day_text;
    };
    LayoutHeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-layout-header',
            template: __webpack_require__("./src/app/layout-header/layout-header.component.html"),
            styles: [__webpack_require__("./src/app/layout-header/layout-header.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__app_config__["a" /* AppConfig */]])
    ], LayoutHeaderComponent);
    return LayoutHeaderComponent;
}());



/***/ }),

/***/ "./src/app/layout-sidebar/layout-sidebar.component.css":
/***/ (function(module, exports) {

module.exports = ".size-a{\r\n    font-size: 0.8rem;\r\n}\r\n.h-10-px{\r\n    border-top: 4px solid rgba(0,0,0,.1);\r\n}\r\n.h-40-px{\r\n    height: 40px;\r\n}\r\n.h-45-px{\r\n    height: 45px;\r\n}\r\n/*Borde a 3 pixeles gris*/\r\n.border-3px{\r\n    border-right: 2px solid #bbb;\r\n}\r\n.color-gray{\r\n    color: #888;\r\n    font-weight: 600;\r\n}\r\n/*Borde a 1 pixeles gris*/\r\ntable, tr, td{\r\n    border: 1px solid #bbb;\r\n}\r\n.icon-santan{\r\n    color: red;\r\n    background-color: white;\r\n    font-size: 18px;\r\n}\r\n.mt-50{\r\n    margin-top: 50px;\r\n}\r\n.mt-46{\r\n    margin-top: 46px;\r\n}\r\n.sidebar-menu:hover{\r\n    background-color: white;\r\n}"

/***/ }),

/***/ "./src/app/layout-sidebar/layout-sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<aside class=\"main-sidebar mt-46\" >\r\n\r\n  <section class=\"sidebar bg-white\" *ngIf=\"true\">\r\n    <ul class=\"sidebar-menu bg-white\"  data-widget=\"tree\">\r\n      <li class=\"active treeview menu-open bg-white\" >\r\n        <a  class=\"size-a bg-white\">\r\n          <em class=\"icon-Factura icon-santan\" ></em> \r\n          <span class=\" bg-white\" >{{constants.SIDER_MENU_FACT}}</span>\r\n          <span class=\"pull-right-container\">\r\n              <em class=\"fa fa-angle-left pull-right\"></em>\r\n            </span>\r\n        </a>\r\n        <ul class=\"treeview-menu bg-white\" >\r\n          <li><a routerLink=\"/factura_consultas\" routerLinkActive=\"active\">{{constants.SIDER_MENU_CON}}</a></li>\r\n        </ul>\r\n      </li>\r\n      \r\n    </ul>\r\n    \r\n  </section>\r\n  \r\n</aside>\r\n\r\n  \r\n  \r\n\r\n\r\n<div class=\"control-sidebar-bg\" ></div>"

/***/ }),

/***/ "./src/app/layout-sidebar/layout-sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LayoutSidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_constants__ = __webpack_require__("./src/model/constants.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LayoutSidebarComponent = /** @class */ (function () {
    function LayoutSidebarComponent(appComponent) {
        this.appComponent = appComponent;
        this.constants = __WEBPACK_IMPORTED_MODULE_2__model_constants__["a" /* Constants */];
    }
    LayoutSidebarComponent.prototype.ngOnInit = function () {
    };
    LayoutSidebarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-layout-sidebar',
            template: __webpack_require__("./src/app/layout-sidebar/layout-sidebar.component.html"),
            styles: [__webpack_require__("./src/app/layout-sidebar/layout-sidebar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__app_component__["a" /* AppComponent */]])
    ], LayoutSidebarComponent);
    return LayoutSidebarComponent;
}());



/***/ }),

/***/ "./src/app/layout/layout-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LayoutRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__layout_component__ = __webpack_require__("./src/app/layout/layout.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__layout_component__["a" /* LayoutComponent */],
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'factura_consultas', loadChildren: '../dashboard/facturacion-consulta/facturacion-consulta.module#FacturacionConsultaModule' },
        ]
    }
];
var LayoutRoutingModule = /** @class */ (function () {
    function LayoutRoutingModule() {
    }
    LayoutRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */]]
        })
    ], LayoutRoutingModule);
    return LayoutRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/layout.component.css":
/***/ (function(module, exports) {

module.exports = ".h-10-px{\r\n    height: 5px;\r\n    background-color: #bbb;\r\n}\r\n/*Color gris de texto*/\r\n.color-gray{\r\n    color: #888;\r\n}\r\n.border-gray-right{\r\n    border-left: 0;\r\n    border-bottom: 0;\r\n    border-top: 0;\r\n    border-color: #888;\r\n    border-width: 2px;\r\n}\r\n/*borde a 3 pixeles gris*/\r\n.border-3px{\r\n    border-right: 3px solid #bbb;\r\n}"

/***/ }),

/***/ "./src/app/layout/layout.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<div>\r\n\r\n  <app-layout-header></app-layout-header>\r\n  <app-layout-sidebar></app-layout-sidebar>\r\n  <app-layout-footer ></app-layout-footer>\r\n</div>"

/***/ }),

/***/ "./src/app/layout/layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__("./src/app/app.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LayoutComponent = /** @class */ (function () {
    function LayoutComponent(config, appComponent, router) {
        this.config = config;
        this.appComponent = appComponent;
        this.router = router;
        //Mensajes de Error
        this.mensajeError = 'La sesión ha expirado, por favor inicie una nueva desde el portal.';
        this.spinner = false;
    }
    LayoutComponent.prototype.ngOnInit = function () {
        //this.validarPerfilamiento();
        //this.router.navigateByUrl('/');
    };
    LayoutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__("./src/app/layout/layout.component.html"),
            styles: [__webpack_require__("./src/app/layout/layout.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* AppConfig */],
            __WEBPACK_IMPORTED_MODULE_1__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]])
    ], LayoutComponent);
    return LayoutComponent;
}());



/***/ }),

/***/ "./src/app/layout/layout.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutModule", function() { return LayoutModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__layout_component__ = __webpack_require__("./src/app/layout/layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__layout_sidebar_layout_sidebar_component__ = __webpack_require__("./src/app/layout-sidebar/layout-sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__layout_header_layout_header_component__ = __webpack_require__("./src/app/layout-header/layout-header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__layout_footer_layout_footer_component__ = __webpack_require__("./src/app/layout-footer/layout-footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__layout_routing_module__ = __webpack_require__("./src/app/layout/layout-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var LayoutModule = /** @class */ (function () {
    function LayoutModule() {
    }
    LayoutModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_6__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_7__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_5__layout_routing_module__["a" /* LayoutRoutingModule */],
            ],
            providers: [],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__layout_component__["a" /* LayoutComponent */],
                __WEBPACK_IMPORTED_MODULE_2__layout_sidebar_layout_sidebar_component__["a" /* LayoutSidebarComponent */],
                __WEBPACK_IMPORTED_MODULE_3__layout_header_layout_header_component__["a" /* LayoutHeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_4__layout_footer_layout_footer_component__["a" /* LayoutFooterComponent */],
            ]
        })
    ], LayoutModule);
    return LayoutModule;
}());



/***/ }),

/***/ "./src/model/constants.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Constants; });
var Constants;
(function (Constants) {
    /**constantes de headers */
    Constants["HEADER_TITLE"] = "CDFI";
    Constants["HEADER_MENU_ADMIN"] = "ADMINISTRACI\u00D3N";
    Constants["HEADER_MENU_FISCAL"] = "FISCAL";
    Constants["HEADER_MENU_EMISION"] = "EMISI\u00D3N";
    Constants["HEADER_MENU_RECEPCION"] = "RECEPCI\u00D3N";
    Constants["HEADER_MENU_SEGURIDAD"] = "SEGURIDAD";
    /**constantes de menu lateral */
    Constants["SIDER_MENU_USERNAME"] = "USER NAME";
    Constants["SIDER_MENU_PROFILE"] = "PROFILE";
    Constants["SIDER_MENU_FACT"] = "Facturaci\u00F3n";
    Constants["SIDER_MENU_MAN"] = "Manual";
    Constants["SIDER_MENU_MAS"] = "Masiva";
    Constants["SIDER_MENU_CON"] = "Consultas y reportes";
    Constants["SIDER_MENU_CAN"] = "Cancelaci\u00F3n";
    Constants["SIDER_MENU_REC"] = "Recibo de donatarios";
    Constants["SIDER_MENU_NOTAS"] = "Notas de cr\u00E9dito";
    Constants["SIDER_MENU_C_VDIVISAS"] = "C/V Divisas";
    Constants["SIDER_MENU_CONSTANCIAS"] = "Constancias fiscales";
    Constants["SIDER_MENU_COMPLEMENTO"] = "Complemento de pagos";
    Constants["SIDER_MENU_EDO_CTAS"] = "Estados de cuenta";
    /**constantes de footer*/
    Constants["FOOTER_COPYRIGHT"] = "Copyright";
    Constants["FOOTER_COPYRIGHT_SYMBOL"] = "&copy;";
    Constants["FOOTER_COPYRIGHT_YEAR"] = "2019";
    Constants["FOOTER_LOGO"] = "Neoris";
    Constants["FOOTER_DERECHOS"] = ". Todos los derechos reservados.";
})(Constants || (Constants = {}));


/***/ })

});
//# sourceMappingURL=layout.module.chunk.js.map